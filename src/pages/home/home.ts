import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder } from "@angular/forms";

declare var FirebaseDatabasePlugin: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private formGroup: FormGroup;
  private ref: any

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, private loading: LoadingController) {
    this.ref = FirebaseDatabasePlugin.ref("Clientes");
    console.log(this.ref);
    this.formGroup = this.formBuilder.group(
      {
        NOME: [''],
        CPF: [''],
        TELEFONE: ['']
      }
    );
  }

  public async submit() {
    var load = this.loading.create();
    await load.present();
    
    this.ref.child(this.formGroup.value["CPF"]).updateChildren(this.formGroup.value).then(
      success => {
        alert("Dados salvos.");
        load.dismiss();
      },
      fail => {
        load.dismiss();
      }
    );
  }

}
