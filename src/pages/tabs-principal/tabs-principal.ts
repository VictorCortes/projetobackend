import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from "../home/home";
import { AboutPage } from "../about/about";
import { ContactPage } from "../contact/contact";

/**
 * Generated class for the TabsPrincipalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs-principal',
  templateUrl: 'tabs-principal.html',
})
export class TabsPrincipalPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;

  constructor() {

  }
}
