
import { Component } from "@angular/core";
import { NavController, LoadingController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

declare var FirebaseDatabasePlugin: any;

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {
    private formGroup: FormGroup;
    constructor(private navController: NavController, private formBuilder: FormBuilder, private loading: LoadingController) {
        this.formGroup = this.formBuilder.group(
            {
                EMAIL: ['', Validators.required],
                SENHA: ['', Validators.required],
            }
        );
    }

    public acessar() {
        if (this.formGroup.valid) {
            var load = this.loading.create();
            load.present();
            FirebaseDatabasePlugin.signInWithEmailAndPassword(this.formGroup.value["EMAIL"], this.formGroup.value["SENHA"]).then(
                success => {
                    load.dismiss();
                    this.navController.setRoot("TabsPrincipalPage");
                },
                fail => {
                    load.dismiss();
                    alert("Credenciais inválidas.");
                }
            );
        }
        else {
            alert("Credenciais inválidas.");
        }
    }
}