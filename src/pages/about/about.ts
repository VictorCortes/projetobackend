import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

declare var FirebaseDatabasePlugin: any;
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  private ref: any;
  private listaClientes = [];

  constructor(public navCtrl: NavController) {
    this.ref = FirebaseDatabasePlugin.ref("Clientes");
    
  }

}
